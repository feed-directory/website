---
title: "About"
draft: false
---

Feed Directory is an opensource project maintained by Valvin. All source code is availble [here](https://framagit.org/feeddirectory)

## Feed Directory data usage

All data that would be collected won't be used for any commercial usage and will limit data collection to technical and legal usage only.
