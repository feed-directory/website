---
title: "Preparing V0.1"
date: 2021-09-12
draft: false
tags: ["v0.1","report","dev"]
categories: ["news"]
---

This week-end I've spent back time on this project. Unfortunately nothing to show right now.

I've published current source code on the [project's gitlab group](https://framagit.org/feed-directory) and started to plan a **v0.1**.

Milestone is available [here](https://framagit.org/feed-directory/feed-directory-app/-/milestones/1) and the goal of it is really simple:

* import a new feed with an url
* import feeds using an OPML file
* browse known feeds with simple method

So nothing like a search engine! This first step will allow having more feeds on which we can play and test the other features.

If people want to bring me some data before this release they can open an [issue](https://framagit.org/feed-directory/feed-directory-app/-/issues) and attached their opml. As this data could be sensitive when associated to a user the issue could be confidential.

About privacy feeds provided throught OPML won't be related each other and be considered as manual feed submission.

So what is the current stauts of this app I think I'm the only one I could run (at the moment). Sorry of being technical:

* create a database schema using SQLAchemy and Flask-migrate: I've tested sqllite and postgres
* import an opml thanks to `atoma` library. each feed url are submitted to a `feed.new` queue where many worker could do the import using nats.io.
* each feed are now scrapped using `feedparser` library which is compatible with more feeds format and feed global data are saved in database.

Hope I'll release this v0.1 quickly :)