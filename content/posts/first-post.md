---
title: "First post"
date: 2021-04-24
draft: false
tags: ["welcome","initialization"]
categories: ["announcement"]
---

This is my first post about this brand new project **Feed Directory**. I'm Valvin, I'm french native so my english is not perfect and you'll find many mistakes. I hope my writings will be readable.

The goal of this project is to provide in the years to come an Atom / RSS feeds search engine. I'm convinced those feeds will have an important role in the future.

So the basic idea is to create an index of feeds that users will be able to submit. Those feeds will be analyzed and indexed in order people can find them easily based on different criteria.

At the moment I'm writing this post there is nothing yet done. So many things have to be done. This website is a first step. Let's see what it'll come in the future.

